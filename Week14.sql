# Initial time (duration): 1.625 sec
# after index  (duration): 1.453 sec
# after PK	   (duration): 0.328 sec

use uniprot_example ;

select	*	from	proteins ;
delete from proteins;

# Loading data file "insert.txt"

load data local infile "C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week14_insert\\insert.txt"
into table proteins fields terminated by "|";

# 280352 rows affected. After truncation, 273230 rows were inserted

# Test query
select	*
from	proteins
where 	protein_name like "%tumor%" and uniprot_id like "%human%"
order by	uniprot_id ;

# Indexing
create index	uniprot_index	on proteins(uniprot_id) ;
drop index	uniprot_index	on proteins ;
	
# To set batch_size larger
set global innodb_purge_batch_size=10000;
show variables like "%batch%";

# Altering as Primary Key
alter table proteins add constraint pk_proteins primary key (uniprot_id) ;
alter table proteins drop primary key ;