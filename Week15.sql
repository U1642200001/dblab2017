use movie_db ;

# After making schema by forward engineering
# drop primary keys to be able to load data
alter table	countries	drop primary key ;
alter table	directors	drop primary key ;
alter table	genres	drop primary key ;
alter table	languages	drop primary key ;
alter table	movie_directors	drop primary key ;
alter table	movie_stars	drop primary key ;
alter table	movies	drop primary key ;
alter table	producer_countries	drop primary key ;
alter table	stars	drop primary key ;

# Load data 
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\countries.csv' into table countries fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\directors.csv' into table directors fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\genres.csv' into table genres fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\languages.csv' into table languages fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\movie_directors.csv' into table movie_directors fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\movie_stars.csv' into table movie_stars fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\movies.csv' into table movies fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\producer_countries.csv' into table producer_countries fields terminated by ';' ;
load data local infile 'C:\\Users\\ABDULAHAD\\Desktop\\Homework BOX\\CENG2008 DBMS\\Week15\\stars.csv' into table stars fields terminated by ';' ;

# Assign primary keys
alter table	countries	add primary key (country_id);
alter table	directors	add primary key (director_id);
alter table	genres	add primary key (movie_id , genre_name);
alter table	languages	add primary key (movie_id , language_name);
alter table	movie_directors	add primary key (movie_id , director_id);
alter table	movie_stars	add primary key (movie_id , star_id);
alter table	movies	add primary key (movie_id);
alter table	producer_countries	add primary key (movie_id , country_id);
alter table	stars	add primary key (star_id);

# Assign foreign keys
alter table directors add constraint fk_directors_countries1 foreign key (country_id) references countries(country_id) ;
alter table producer_countries add constraint fk_producer_movies1 foreign key (movie_id) references movies(movie_id) ;
alter table producer_countries add constraint fk_producer_countries1 foreign key (country_id) references countries(country_id) ;
alter table languages add constraint fk_languages_movies1 foreign key (movie_id) references movies(movie_id) ;
alter table genres add constraint fk_genres_movies1 foreign key (movie_id) references movies(movie_id) ;
alter table movie_directors add constraint fk_movie_directors_directors1 foreign key (director_id) references directors(director_id) ;
alter table movie_directors add constraint fk_movie_directors_movies1 foreign key (movie_id) references movies(movie_id) ;
alter table movie_stars add constraint fk_movie_stars_movies1 foreign key (movie_id) references movies(movie_id) ;
alter table movie_stars add constraint fk_movie_stars_stars1 foreign key (star_id) references stars(star_id) ;
alter table stars add constraint fk_stars_countries1 foreign key (country_id) references countries(country_id) ;

create index fk_directors_countries1_idx on directors (country_id asc) ;
create index fk_producer_movies1_idx on producer_countries (movie_id asc) ;
create index fk_producer_countries1_idx on producer_countries (country_id asc) ;
create index fk_languages_movies1_idx on languages (movie_id asc) ;
create index fk_genres_movies1_idx on genres (movie_id asc) ;
create index fk_movie_directors_directors1_idx on movie_directors (director_id asc) ;
create index fk_movie_directors_movies1_idx on movie_directors (movie_id asc) ;
create index fk_movie_stars_movies1_idx on movie_stars (movie_id asc) ;
create index fk_movie_stars_stars1_idx on movie_stars (star_id asc) ;
create index fk_stars_countries1_idx on stars (country_id asc) ;
